pragma solidity 0.4.25;

contract BizarreBazaar {

  struct Profile {
    bool registered;
    bytes32 whisper;
    bytes32 ipfs;
  }

  mapping(address => Profile) public profiles;
  address[] public users;
  uint256 public totalUser;

  function getProfileByIndex(uint256 userId) 
  public 
  view
  returns(bytes32, bytes32) {
    Profile storage a = profiles[users[userId]];
    return (a.whisper, a.ipfs);
  }

  function setWhisper(bytes32 whisper) public {
    maybeRegister(msg.sender);
    profiles[msg.sender].whisper = whisper;
  }

  function setIpfs(bytes32 ipfs) public {
    maybeRegister(msg.sender);
    profiles[msg.sender].ipfs = ipfs;
  }

  function delegateSetWhisper(bytes32 whisper, address user, uint8 v, bytes32 r, bytes32 s) public {
    // The user we're summiting the profile for must have signed off on this whisper id
    require(isValidSignature(whisper, user, v, r, s));
    maybeRegister(user);
    profiles[user].whisper = whisper;
  }

  function delegateSetIpfs(bytes32 ipfs, address user, uint8 v, bytes32 r, bytes32 s) public {
    // The user we're summiting the profile for must have signed off on this ipfs hash
    require(isValidSignature(ipfs, user, v, r, s));
    maybeRegister(user);
    profiles[user].ipfs = ipfs;
  }

  function maybeRegister(address user) internal {
    if (!profiles[user].registered) {
      users.push(user);
      profiles[user].registered = true;
      totalUser +=1;
    }
  }

  function isValidSignature(bytes32 message, address signer, uint8 v, bytes32 r, bytes32 s) internal pure returns (bool _valid) {
    return signer == ecrecover(keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", message)), v, r, s);
  }

  // contract should not be payable by default
  function () public { }
}
