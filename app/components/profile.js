import EmbarkJS from 'Embark/EmbarkJS';
import base58 from 'bs58';
import BN from 'bn.js';
import React from 'react';
import { Form, FormGroup, FormControl, HelpBlock, Button } from 'react-bootstrap';
import BizarreBazaar from 'Embark/contracts/BizarreBazaar';

import Listing from './listing.js';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      myAddress: 'loading..',
      ipfsSet: '',
      ipfsGet: '',
      whisperSet: '',
      whisperGet: '',
      listings: [],
      listingsHash: '',
      logs: []
    }
  }

  handleTitleChange(e) { this.setState({ setTitle: e.target.value }); }
  handleDescriptionChange(e) { this.setState({ setDescription: e.target.value }); }
  checkEnter(e, func) { if (e.key !== 'Enter') return; e.preventDefault(); func.apply(this, [e]); }

  componentDidMount() {
    EmbarkJS.onReady(async (err) => {
      if (err) { console.error(err); return; }
      const accounts = await web3.eth.getAccounts();
      this.setState({ myAddress: accounts[0] });
      this.getListings();
    });
  }

  async getListings() {
    var address = this.state.myAddress;
    var value = await BizarreBazaar.methods.profiles(address).call()
    this.addToLog(`BizarreBazaar.methods.profiles(${address}).call()`);
    if (value.ipfs.substr(2) == '0'*40) {
      var listingsHash = ''
      var listings = [];
    } else {
      var listingsHash = base58.encode(Buffer.from('1220' + value.ipfs.substr(2), 'hex'));
      var listings = JSON.parse(await EmbarkJS.Storage.get(listingsHash));
    }
    this.setState({ listingsHash, listings });
  }

  async setIpfs(e) {
    e.preventDefault();
    var newListings = this.state.listings.concat({
      title: this.state.setTitle,
      description: this.state.setDescription
    })
    console.log(JSON.stringify(newListings))
    var ipfsHash = await EmbarkJS.Storage.saveText(JSON.stringify(newListings))
    console.log(`Uploading /ipf/${ipfsHash}`)
    // the first 2 bytes designate the hash function. cut them off
    var toSet = '0x' + base58.decode(ipfsHash).toString('hex').substr(4)
    var receipt = await BizarreBazaar.methods.setIpfs(toSet).send({ gas: 160000 });
    console.log(receipt)
    this.addToLog(`BizarreBazaar.methods.setIpfs(${toSet}).send();`);
    this.setState({ setTitle: '', setDescription: '' });
    this.getListings()
  }

  addToLog(txt) {
    this.state.logs.push(txt);
    this.setState({ logs: this.state.logs });
  }

  render() {
    return (<div>
      <h4>Profile for account: {this.state.myAddress}</h4>

      <h4>Submit a new listing</h4>
      <Form inline onKeyDown={(e) => this.checkEnter(e, this.setIpfs)}>
        <FormGroup>
          <HelpBlock>Title</HelpBlock>
          <FormControl type="text" defaultValue={this.state.setTitle} onChange={(e) => this.handleTitleChange(e)}/>
          <HelpBlock>Description</HelpBlock>
          <FormControl type="text" defaultValue={this.state.setDescription} onChange={(e) => this.handleDescriptionChange(e)}/>
          <HelpBlock>Save data to the blockchain</HelpBlock>
          <Button bsStyle="primary" onClick={(e) => this.setIpfs(e)}>Submit New Listing</Button>
        </FormGroup>
      </Form>

      <h4>Your Listings</h4>
      <div>
        <p> list of listings... </p>
        {this.state.listings.map((listing, index) => <Listing key={index} listing={listing} />)}
        <p>{JSON.stringify(this.state.listings)}</p>
      </div>

      <h4>Debug Zone</h4>
      <div className="logs">
        {JSON.stringify(this.state,null,2)}
        {this.state.logs.map((item, i) => <p key={i}>{item}</p>)}
      </div>

    </div>
    );
  }
}
