import EmbarkJS from 'Embark/EmbarkJS';
import BizarreBazaar from 'Embark/contracts/BizarreBazaar';
import React from 'react';
import { Form, FormGroup, FormControl, HelpBlock, Button } from 'react-bootstrap';

export default function Listing(props) {
  const listing = props.listing
  return (<div>

    <h4>{listing.title}</h4>
    <p>{listing.description}</p>
    <hr/>

  </div>);

}
