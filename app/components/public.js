import EmbarkJS from 'Embark/EmbarkJS';
import BizarreBazaar from 'Embark/contracts/BizarreBazaar';
import React from 'react';
import { Form, FormGroup, FormControl, HelpBlock, Button } from 'react-bootstrap';
import Listing from './listing.js';
import base58 from 'bs58';

class Public extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profiles: [],
      listings: [],
      logs: []
    }
  }

  async componentDidMount() {
    EmbarkJS.onReady(async (err) => {
      if (err) { console.error(err); return; }
      let _total = await BizarreBazaar.methods.totalUser().call()
      this.addToLog(`BizarreBazaar.methods.totalUser().call()`);
      console.log(_total)

      for (let i = 0; i < _total; i++) {
        let _profile = await BizarreBazaar.methods.getProfileByIndex(i).call()
        const ipfs = _profile[1]
        const whisper = _profile[0]
        this.setState((state) => ({ 
          profiles: state.profiles.concat({ whisper, ipfs })
        }))

        if (ipfs != null) {
          var listingsHash = base58.encode(Buffer.from('1220' + ipfs.substr(2), 'hex'));
          var _listings = JSON.parse(await EmbarkJS.Storage.get(listingsHash));
          this.setState((state) => ({
            listings: state.listings.concat(_listings)
          }))
        }
      }
    });
  }

  addToLog(txt) {
    this.state.logs.push(txt);
    this.setState({ logs: this.state.logs });
  }

  render() {
    return (
      <div>

      {this.state.listings.map((listing, i) =>
        <Listing key={i} listing={listing} />
      )}

      <h4>Debug Zone</h4>
      <div className="logs">
        {JSON.stringify(this.state,null,2)}
        {this.state.logs.map((item, i) => <p key={i}>{item}</p>)}
      </div>
    </div>


    )
  }
}

export default Public;
